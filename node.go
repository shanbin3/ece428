package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
)

func main() {
	if len(os.Args) != 4 {
		fmt.Printf("Arguments should follow the format: Node Name, Node IP, Node Port\n")
		return
	}

	nodeName := os.Args[1]
	nodeIP := os.Args[2]
	nodePort := os.Args[3]
	parsedIP := net.ParseIP(nodeIP)
	buf := make([]byte, 1024)

	if parsedIP == nil {
		fmt.Printf("Invaild IP address!\n")
		return
	}

	nodeAddr := nodeIP + ":" + nodePort
	nodeConn, err := net.Dial("tcp", nodeAddr)

	if err != nil {
		fmt.Printf("Unable to assign the %s with the address %s\n", nodeName, nodeAddr)
		fmt.Printf("%s\n",err)
		return
	}

	defer func(){
		nodeConn.Close()
		fmt.Printf("%s close\n", nodeName)
	}()

	fmt.Fprintf(nodeConn, "%s begin\n", nodeName)
	fmt.Println("sent out handshake message")
	n, err := nodeConn.Read(buf)
	if string(buf[:n])=="ack\n" {
		fmt.Printf("Connected to the logger successfully.\n")
	} else {
		fmt.Printf("Unable to get ack from logger.\n")
		return
	}

	scanner := bufio.NewScanner(os.Stdin)
	counter := 0
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Count(line, " ") != 1{
			fmt.Printf("%s received an invalid event and discarded it.\n",nodeName)
			continue
		} 
		counter++
		fmt.Printf("%d %s %s\n", counter, nodeName, line)
		fmt.Fprintf(nodeConn, "%s %s\n", nodeName, line)
		_, err := nodeConn.Read(buf)
		if err != nil {
			fmt.Printf("Unable to get message from logger\n")
			fmt.Printf("%s\n", err)
			return
		}
		if string(buf[:n])!="ack\n" {
			fmt.Printf("Unable to get ack from logger.\n")
			return
		}
	}
	return
}
