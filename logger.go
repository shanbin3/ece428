package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

var logfilename string = "log"
var metricfilename string = "metric"

func main() {
	// var filename string
	var portnumber string
	//check the arguments
	if len(os.Args) != 2 {
		fmt.Printf("Argument as follows: ./logger port\n")
		os.Exit(1)
	}
	// filename = os.Args[0]
	portnumber = os.Args[1]

	logger, err := net.Listen("tcp", "0.0.0.0:"+portnumber)
	if err != nil {
		fmt.Println("Error creating logger:", err)
		os.Exit(1)
	}
	defer logger.Close()
	fmt.Printf("The logger is listening on %s\n", logger.Addr().String())

	//create the concurrent waiter
	var waiter sync.WaitGroup

	logfile, err := os.OpenFile(logfilename, os.O_TRUNC|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		println("Can't create a log file")
		os.Exit(1)
	}

	defer logfile.Close()
	metricfile, _ := os.OpenFile(metricfilename, os.O_TRUNC|os.O_CREATE|os.O_WRONLY, 0644)
	metricfile.WriteString(fmt.Sprintf("node,sendtime,receivetime,textlength\n"))
	defer metricfile.Close()

	//accept message
	for {
		connection, err := logger.Accept()
		if err != nil {
			fmt.Println("Error accepting connection:", err)
			continue
		}

		waiter.Add(1)
		go handle_node(connection, &waiter)

	}

}

func handle_node(connection net.Conn, waiter *sync.WaitGroup) {
	defer connection.Close()
	defer waiter.Done()
	var name string
	receive := bufio.NewScanner(connection)

	//logfile
	logfile, err := os.OpenFile(logfilename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		println("Can't create a log file")
		os.Exit(1)
	}
	defer logfile.Close()

	//create the csv
	// file, err := os.Create(metricfilename)
	// if err != nil {
	// 	panic(err)
	// }
	// defer file.Close()
	// writer := csv.NewWriter(file)
	// defer writer.Flush()

	//handle each event
	for receive.Scan() {
		text := receive.Text()

		// connection.Write([]byte("ack\n"))
		// fmt.Println("A")
		fmt.Fprint(connection, "ack\n")
		//the case then it starts to connect
		if strings.Contains(text, "begin") {
			nodename := strings.Fields(text)[0]
			name = nodename
			timeStr := strconv.FormatFloat(gettime(time.Now()), 'f', 7, 64)
			logfile.WriteString(fmt.Sprintf("%s - %s connected\n", timeStr, nodename))
			fmt.Printf("%s - %s connected\n", timeStr, nodename)
		} else {
			nodename := strings.Fields(text)[0]
			receivetime := gettime(time.Now())
			sendtime, _ := strconv.ParseFloat(strings.Fields(text)[1], 64)
			msg := strings.Fields(text)[2]

			//write to the log file
			timeStr := strconv.FormatFloat(receivetime, 'f', 7, 64)
			logfile.WriteString(fmt.Sprintf("%s %s %s\n", timeStr, nodename, msg))
			fmt.Printf("%s %s %s\n", timeStr, nodename, msg)
			// dif := receivetime - sendtime
			// delay := strconv.FormatFloat(dif, 'f', 12, 64)
			// bandwidth := strconv.FormatFloat(float64(len(text))/dif, 'f', 12, 64)

			//write to the metric file

			// data := []string{nodename, delay, bandwidth}

			// if err := writer.Write(data); err != nil {
			// 	panic(err)
			// }

			metricfile, errm := os.OpenFile(metricfilename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
			if errm != nil {
				println("Can't create a metric file")
				os.Exit(1)
			}
			metricfile.WriteString(fmt.Sprintf("%s,%s,%s,%s\n", nodename, strconv.FormatFloat(sendtime, 'f', 12, 64), strconv.FormatFloat(receivetime, 'f', 12, 64), strconv.FormatInt(int64(len(text)), 10)))
			metricfile.Close()

		}
	}
	// enderr := receive.Err()
	// if enderr != nil {
	// 	if enderr == io.EOF {
	// 		logfile.WriteString(fmt.Sprintf("%s - %s disconnected\n", strconv.FormatFloat(gettime(time.Now()), 'f', 7, 64), name))
	// 	} else {
	// 		fmt.Println("Error reading from connection:", err)
	// 		return
	// 	}
	// }
	timeStr := strconv.FormatFloat(gettime(time.Now()), 'f', 7, 64)
	logfile.WriteString(fmt.Sprintf("%s - %s disconnected\n", timeStr, name))
	fmt.Printf("%s - %s disconnected\n", timeStr, name)

}

func gettime(thistime time.Time) float64 {
	now := thistime
	seconds := now.Unix()
	nanoSeconds := now.Nanosecond()
	decimalSeconds := float64(nanoSeconds) / 1e9
	totalSeconds := float64(seconds) + decimalSeconds
	return totalSeconds
}
